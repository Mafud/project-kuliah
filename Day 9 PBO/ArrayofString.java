/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-05-11 04:04:07
 * @modify date 2020-05-11 04:04:07
 * @desc [description]
 */
public class ArrayofString {
    public static void main(String[] args) {
        String[] anArray = { "String one", "String two", "String threee" };
        for (int i = 0; i < anArray.length; i++) {
            System.out.println(anArray[i].toLowerCase());
        }
    }
}