/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-05-11 04:03:54
 * @modify date 2020-05-11 04:03:54
 * @desc [description]
 */
public class ArrayOfarraysDemo2 {
    public static void main(String[] args) {
        int[][] aMatrix = new int[4][];
        // fpopulate matrix
        for (int i = 0; i < aMatrix.length; i++) {
            aMatrix[i] = new int[5]; // create sub-array
            for (int j = 0; j < aMatrix[i].length; j++) {
                aMatrix[i][j] = i + j;
            }
        }
        // print matrix
        for (int i = 0; i < aMatrix.length; i++) {
            for (int j = 0; j < aMatrix[i].length; j++) {
                System.out.print(aMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}