/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-05-11 04:04:01
 * @modify date 2020-05-11 04:04:01
 * @desc [description]
 */
public class ArrayDemo {
    public static void main(String[] args) {
        int[] anArray;
        anArray = new int[10];
        for (int i = 0; i < anArray.length; i++) {
            anArray[i] = i;
            System.out.println(anArray[i] + " ");
        }
        System.out.println();
    }
}