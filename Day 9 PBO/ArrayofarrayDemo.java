/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-05-11 04:04:22
 * @modify date 2020-05-11 04:04:22
 * @desc [description]
 */
public class ArrayofarrayDemo {
    public static void main(String[] args) {
        String[][] cartoons = { { "Flinstones", "Fred", "Wilma", "Pebbles", "Dino" },
                { "Rubbles", "Barney", "Betty", "Bam Bam" },
                { "Jetsons", "George", "Jane", "Elroy", "Judy", "Rosie", "Astro" },
                { "Scooby Doo Gang", "Scooby Doo", "Shaggy", "Velma", "Fred", "Daphne" } };
        for (int i = 0; i < cartoons.length; i++) {
            System.out.println(cartoons[i][0] + "");
            for (int j = 1; j < cartoons[i].length; j++) {
                System.out.println(cartoons[i][j] + "");
            }
            System.out.println();
        }
    }

}
