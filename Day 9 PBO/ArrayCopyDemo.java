/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-05-11 04:04:35
 * @modify date 2020-05-11 04:04:35
 * @desc [description]
 */
public class ArrayCopyDemo {
        public static void main(String[] args) {
        char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e','i','n','a','t','e','d'};
        char[] copyTo = new char[7];
        System.arraycopy(copyFrom, 2, copyTo, 0, 7);
        System.out.println(new String(copyTo) );
        }
}