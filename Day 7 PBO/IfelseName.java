/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 21:44:50
 * @modify date 2020-04-25 21:44:50
 * @desc [description]
 */
class IfelseName {
    public static void main(String args[]) {
        char firstlnitial = (char) -1;
        System.out.println("Enter your ﬁrst initial:");
        try {
            firstlnitial = (char) System.in.read();
        } catch (Exception e) {
            System.out.println("Error:" + e.toString());
        }

        if (firstlnitial == 'a')
            System.out.println("Your name must be Ali ! ");
        else if (firstlnitial == 'b')
            System.out.println("Your name must be Brodin! ");
        else if (firstlnitial == 'c')
            System.out.println("Your name must be Cecep! ");
        else
            System.out.println("I can't ﬁgure out your name ! ");
    }
}