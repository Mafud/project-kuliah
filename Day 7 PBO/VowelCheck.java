/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 21:52:32
 * @modify date 2020-04-25 21:52:32
 * @desc [description]
 */
public class VowelCheck {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Usage: java VowelCheck <char>");
            System.exit(1);
        }
        char c = args[0].charAt(0);
        switch (c) {
            case 'a':
            case 'A':
            case 'E':
            case 'i':
            case 'I':
            case 'o':
            case 'O':
            case 'u':
            case 'U':
                System.out.println(c + " is a Vowel.");
                break;
            case 'y':
            case 'Y':
            case 'w':
            case 'W':
                System.out.println(c + " is sometimes a vowel.");
                break;
            default:
                System.out.println(c + " is not a v0wel.");
        }
    }
}