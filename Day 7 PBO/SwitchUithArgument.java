/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 22:00:06
 * @modify date 2020-04-25 22:00:06
 * @desc [description]
 */
public class SwitchUithArgument {
    public static void main(String[] args) {
        if (args.length != 3) {
            System.out.println("argumen <tgl> <bulan> <tahun>");
            System.exit(1);
        }
        int tgl = (Integer.valueOf(args[0])).intValue();
        int bulan = (Integer.valueOf(args[1])).intValue();
        int tahun = (Integer.valueOf(args[2])).intValue();

        switch (bulan) {
            case 1:
                System.out.println(tgl + " January " + tahun);
                break;
            case 2:
                System.out.println(tgl + " February " + tahun);
                break;
            case 3:
                System.out.println(tgl + " March " + tahun);
                break;
            case 4:
                System.out.println(tgl + " April " + tahun);
                break;
            case 5:
                System.out.println(tgl + " May " + tahun);
                break;
            case 6:
                System.out.println(tgl + " June " + tahun);
                break;
            case 7:
                System.out.println(tgl + " July " + tahun);
                break;
            case 8:
                System.out.println(tgl + " August " + tahun);
                break;
            case 9:
                System.out.println(tgl + " September " + tahun);
                break;
            case 10:
                System.out.println(tgl + " October " + tahun);
                break;
            case 11:
                System.out.println(tgl + " November " + tahun);
                break;
            case 12:
                System.out.println(tgl + " December " + tahun);
                break;
            default:
                System.out.println("Invalid month.");
                break;

        }

    }
}