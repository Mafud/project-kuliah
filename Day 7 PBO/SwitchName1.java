/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 21:44:42
 * @modify date 2020-04-25 21:44:42
 * @desc [description]
 */
class SwitchName1 {
    public static void main(String[] args) {
        char ﬁrstlnitial = (char) -1;
        System.out.println("Enter your ﬁrstlnitial: ");
        try {
            ﬁrstlnitial = (char) System.in.read();
        } catch (Exception e) {
            System.out.println("Error:" + e.toString());
        }

        switch (ﬁrstlnitial) {
            case 'a':
                System.out.println("Your name must be Ali !");
            case 'b':
                System.out.println("Your name must be Brodin! ");
            case 'c':
                System.out.println("Your name must be Cecep! ");
            default:
                System.out.println("I can't ﬁgure out your name ! ");
        }
    }
}