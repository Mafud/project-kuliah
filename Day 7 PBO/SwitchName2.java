/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 21:44:25
 * @modify date 2020-04-25 21:44:25
 * @desc [description]
 */
class SwitchName2 {
    public static void main(String args[]) {
        char firstInitial = (char) -1;
        System.out.println("Enter your firstInitial");
        try {
            firstInitial = (char) System.in.read();
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
        }
        switch (firstInitial) {
            case 'a':
                System.out.println("Your name must be Ali! ");
                break;
            case 'b':
                System.out.println("Your name must be Brodinl ");
                break;
            case 'c':
                System.out.println("Your name must be Cecepl I");
                break;
            default:
                System.out.println("I can't ﬁgure out your name! ");
        }
    }
}