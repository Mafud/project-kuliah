/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 21:42:41
 * @modify date 2020-04-25 21:42:41
 * @desc [description]
 */
class SwitchDemo2 {
    public static void main(String[] args) {
        int month = 2;
        int Year = 2000;
        int numDays = 0;
        switch (month) {
            case 3:
            case 5:
            case 8:
            case 10:
            case 12:
                numDays = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                numDays = 30;
                break;
            case 2:
                if (((Year % 4 == 0) && (Year % 100 == 0)) || (Year % 400 == 0))
                    numDays = 29;
                else
                    numDays = 28;
                break;
            default:
                System.out.println("Invalid month.");
                break;
        }
        System.out.println("Number of Days = " + numDays);
    }
}