/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 22:19:38
 * @modify date 2020-04-25 22:40:34
 * @desc [description]
 */
public class continuedemo {
    public static void main (String args[]){
        String searchme="peter piper picked a peck of pickled peppers";
        int max= searchme.length();
        int numPs=0;
        for ( int i=0; i <max; i++){
            if (searchme.charAt(i)!='p')
                continue;
            numPs++;
        }
        System.out.println("Found "+numPs+" p's in the string.");
    }
}