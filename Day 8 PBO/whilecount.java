/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 22:16:33
 * @modify date 2020-04-25 22:16:33
 * @desc [description]
 */

public class whilecount {
    public static void main(String args[]) {
        char input = (char) -1;
        int numToCount;
        System.out.println("Enter number to count to between 0 and 10 :");
        try {
            input = (char) System.in.read();
        } catch (Exception e) {
            System.out.println("Eror :" + e.toString());
        }
        numToCount = Character.digit(input, 10);
        if ((numToCount > 0) && (numToCount < 11)) {
            int i = 1;
            while (i <= numToCount) {
                System.out.println(i);
                i++;
            }
        } else
            System.out.println("That number not between 0 and 10");
    }
}