/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 22:28:28
 * @modify date 2020-04-25 22:40:40
 * @desc [description]
 */
public class continuewithlabeldemo {
    public static void main (String args[]){
        String searchme ="Look for a substring in me ";
        String substring="Sub";
        boolean foundit=false;
        int max = searchme.length()-substring.length();
        
        test :
        for (int i =0;i<=max;i++){
            int n=substring.length();
            int j=i;
            int k=0;
            while (n-- !=0){
                if(searchme.charAt(j++)
                        !=substring.charAt(k++)){
                    continue test;
                }
            }
            foundit=true;
            break test;
        }
        System.out.println(foundit?"Found it":"Didn't find it");
    }
}