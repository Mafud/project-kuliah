/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-25 22:17:18
 * @modify date 2020-04-25 22:17:18
 * @desc [description]
 */

public class whiledemo {
    public static void main(String args[]) {
        int count = 1;
        while (count < 11) {
            System.out.println("Count is:" + count);
            count++;
        }
    }
}

class DoWhileDemo {
    public static void main(String args[]) {
        int count = 1;
        do {
            System.out.println("Count is:" + count);
            count++;
        } while (count <= 11);
    }
}