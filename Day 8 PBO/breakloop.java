/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-04-30 22:34:58
 * @modify date 2020-04-30 22:34:58
 * @desc [description]
 */

public class breakloop {
    public static void main (final String args[]) {
        int i=0;
        do{
            System.out.println("I'm Stuck!");
            i++;
            if (i>10)break;
        }
        while (true);
    }
}