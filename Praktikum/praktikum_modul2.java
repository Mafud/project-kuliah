/**
 * @author MafudNK
 * @email mafudnurkarim@gmail.com
 * @create date 2020-05-07 21:00:13
 * @modify date 2020-05-07 21:00:13
 * @desc [description]
 */

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.util.Scanner;
import java.io.IOException;
public class praktikum_modul2 {
   
   static void notepad(String kata)throws IOException {
      FileOutputStream fos = null;
      FilterOutputStream  os = null;
      FileDescriptor fd = null;
      boolean bool = false;
      try {
         // create new file output stream
         fos = new FileOutputStream("D://test.txt");
         os = new FilterOutputStream(fos);
         
         //input ke file.txt
         os.write(kata.getBytes());
         os.flush();

         // get file descriptor instance
         fd = fos.getFD();
         
         // test if the file is valid
         bool = fd.valid();
      
         // print
         System.out.println("Is file valid? "+bool);
         
      } catch(Exception ex) {
         // if an error occurs
         ex.printStackTrace();
      } finally {
         if(fos!=null)
            fos.close();
      }
   }
   public static void main(String[] args) throws IOException {
      String text = "";
      Scanner sc = new Scanner(System.in);
      System.out.print("masukkan kata ke dalam file notepad = ");
      text = sc.nextLine();
      
      notepad(text);
   }
}
