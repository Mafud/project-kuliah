/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package latihan_uts;

/**
 *
 * @author Mafud
 */
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.*;
import static java.lang.Double.NaN;
import static java.lang.Math.*;
//import static java.lang.Math.log10;
//import static java.lang.Math.pow;
//import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
//import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.JTextArea;
//import java.awt.Font;
//import javax.swing.Box;
//import javax.swing.BoxLayout;
//import java.awt.Image;
//import javax.swing.ImageIcon;
//import java.io.*;
//import javax.swing.JLabel;
//import javax.swing.SwingConstants;

class Calculator {

    public enum BiOperatorModes {
        normal, add, minus, multiply, divide, xpowerofy
    }
    private Double num1, num2;
    private BiOperatorModes mode = BiOperatorModes.normal;

    private Double calculateBiImpl() {
        if (mode == BiOperatorModes.normal) {
            return num2;
        }
        if (mode == BiOperatorModes.add) {
            if (num2 != 0) {
                return num1 + num2;
            }

            return num1;
        }
        if (mode == BiOperatorModes.minus) {
            return num1 - num2;
        }
        if (mode == BiOperatorModes.multiply) {
            return num1 * num2;
        }
        if (mode == BiOperatorModes.divide) {
            return num1 / num2;
        }
        if (mode == BiOperatorModes.xpowerofy) {
            return pow(num1, num2);
        }

        throw new Error();
    }

    public Double calculateBi(BiOperatorModes newMode, Double num) {
        if (mode == BiOperatorModes.normal) {
            num2 = 0.0;
            num1 = num;
            mode = newMode;
            return NaN;
        } else {
            num2 = num;
            num1 = calculateBiImpl();
            mode = newMode;
            return num1;
        }
    }

    public Double calculateEqual(Double num) {
        return calculateBi(BiOperatorModes.normal, num);
    }

    public Double reset() {
        num2 = 0.0;
        num1 = 0.0;
        mode = BiOperatorModes.normal;

        return NaN;
    }


}

class UI implements ActionListener {

    private final JFrame frame;

    private final JPanel panel;
    private final JPanel panelSub1;
    private final JPanel panelSub2;
    private final JPanel panelSub3;
    private final JPanel panelSub4;
    private final JPanel panelSub5;
    private final JPanel panelSub6;
    private final JPanel panelSub7;
    private final JPanel panelSub8;

    private final JTextArea text;
    private final JButton but[], butAdd, butMinus, butMultiply, butDivide,
            butEqual, butCancel, butComa;
    private final Calculator calc;

    private final String[] buttonValue = {"0", "1", "2", "3", "4", "5", "6",
        "7", "8", "9"};

    private final Font font;
    private final Font textFont;
    private ImageIcon image;

    public UI() throws IOException {
        frame = new JFrame("");
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panelSub1 = new JPanel(new FlowLayout());
        panelSub2 = new JPanel(new FlowLayout());
        panelSub3 = new JPanel(new FlowLayout());
        panelSub4 = new JPanel(new FlowLayout());
        panelSub5 = new JPanel(new FlowLayout());
        panelSub6 = new JPanel(new FlowLayout());
        panelSub7 = new JPanel(new FlowLayout());
        panelSub8 = new JPanel(new FlowLayout());
        font = new Font("Consolas", Font.PLAIN, 15);
        text = new JTextArea(1, 25);
        textFont = new Font("Consolas", Font.PLAIN, 18);
        but = new JButton[100];
        for (int i = 0; i < 10; i++) {
            but[i] = new JButton(String.valueOf(i));
        }
        butAdd = new JButton("+");
        butMinus = new JButton("-");
        butMultiply = new JButton("X");
        butDivide = new JButton("/");
        butEqual = new JButton("=");
        butCancel = new JButton("C");
        butComa = new JButton(",");

        calc = new Calculator();

    }

    public void init() {
        Dimension d = new Dimension(90, 30);
        frame.setSize(350, 300);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        text.setFont(textFont);
        text.setEditable(false);

        for (int i = 0; i < 10; i++) {
            but[i].setFont(font);
        }
        butAdd.setFont(font);
        butMinus.setFont(font);
        butMultiply.setFont(font);
        butDivide.setFont(font);
        butEqual.setFont(font);
        butCancel.setFont(font);
        butComa.setFont(font);
        panel.add(Box.createHorizontalStrut(100));
        panelSub1.add(text);
        panel.add(panelSub1);

        panelSub2.add(but[1]);
        panelSub2.add(but[2]);
        panelSub2.add(but[3]);
        panelSub2.add(Box.createHorizontalStrut(15));
        butCancel.setPreferredSize(d);
        panelSub2.add(butCancel);
        panel.add(panelSub2);

        panelSub3.add(but[4]);
        panelSub3.add(but[5]);
        panelSub3.add(but[6]);
        panelSub3.add(Box.createHorizontalStrut(15));
        panelSub3.add(butAdd);
        panelSub3.add(butMultiply);
        panel.add(panelSub3);

        panelSub4.add(but[7]);
        panelSub4.add(but[8]);
        panelSub4.add(but[9]);
        panelSub4.add(Box.createHorizontalStrut(15));
        panelSub4.add(butMinus);
        panelSub4.add(butDivide);
        panel.add(panelSub4);

        butEqual.setPreferredSize(d);
        but[0].setPreferredSize(d);
        panelSub5.add(butComa);
        panelSub5.add(but[0]);
        panelSub5.add(Box.createHorizontalStrut(15));
        panelSub5.add(butEqual);
        panel.add(panelSub5);

        for (int i = 0; i < 10; i++) {
            but[i].addActionListener(this);
        }
        butAdd.addActionListener(this);
        butMinus.addActionListener(this);
        butMultiply.addActionListener(this);
        butDivide.addActionListener(this);
        butEqual.addActionListener(this);
        butCancel.addActionListener(this);

        frame.add(panel);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final Object source = e.getSource();

        for (int i = 0; i < 10; i++) {
            if (source == but[i]) {
                text.replaceSelection(buttonValue[i]);
                return;
            }
        }

        if (source == butAdd) {
            writer(calc.calculateBi(Calculator.BiOperatorModes.add, reader()));
        }

        if (source == butMinus) {
            writer(calc.calculateBi(Calculator.BiOperatorModes.minus, reader()));
        }

        if (source == butMultiply) {
            writer(calc.calculateBi(Calculator.BiOperatorModes.multiply,
                    reader()));
        }

        if (source == butDivide) {
            writer(calc
                    .calculateBi(Calculator.BiOperatorModes.divide, reader()));
        }
        if (source == butEqual) {
            writer(calc.calculateEqual(reader()));
        }

        if (source == butCancel) {
            writer(calc.reset());
        }

        text.selectAll();
    }

    private void parsetoBinary() {
        try {
            text.setText("" + Long.toBinaryString(Long.parseLong(text.getText())));
        } catch (NumberFormatException ex) {
            System.err.println("Error while parse to binary." + ex.toString());
        }
    }

    public Double reader() {
        Double num;
        String str;
        str = text.getText();
        num = Double.valueOf(str);

        return num;
    }

    public void writer(final Double num) {
        if (Double.isNaN(num)) {
            text.setText("");
        } else {
            text.setText(Double.toString(num));
        }
    }
}

public class TesCal2 {

    public static void main(String[] args) {
        try {
            UI uiCal = new UI();
            uiCal.init();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}