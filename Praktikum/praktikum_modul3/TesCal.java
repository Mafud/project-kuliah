package latihan_uts;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

class Convert extends JFrame {

    String a;
    String fromTemp, toTemp;
    double result, ans;
    JTextField jtx1 = new JTextField();
    JTextField jtx2 = new JTextField();
    Container cnt = new Container();
    Font fnt = new Font("Consolas", Font.PLAIN, 15);
    Font fnt1 = new Font("Consolas", Font.PLAIN, 18);
    String[] listTemp = {"Celcius", "Kelvin", "Fahrenheit"};
    JComboBox cb = new JComboBox(listTemp);
    JComboBox cb2 = new JComboBox(listTemp);
    JButton bt = new JButton("Ok");
    JLabel lbl1 = new JLabel("Program Konversi Suhu");
    JLabel lbl2 = new JLabel("Hasil");
    JLabel lbl3 = new JLabel("Ke");

    Convert() {
        make_frame();
        set_component();
        set_action();
    }

    void make_frame() {
        this.setVisible(true);
        this.setResizable(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBounds(500, 200, 400, 400);
        this.setTitle("");
        this.setLayout(null);
    }

    void set_component() {
        cnt = this.getContentPane();
        jtx1.setFont(fnt1);
        jtx2.setFont(fnt1);
        lbl1.setBounds(50, 50, 200, 50);
        jtx1.setBounds(50, 100, 300, 30);
        cb.setBounds(50, 150, 95, 30);
        lbl3.setBounds(165, 150, 20, 30);
        cb2.setBounds(195, 150, 95, 30);
        bt.setBounds(300, 150, 50, 30);
        lbl2.setBounds(50, 200, 100, 50);
        jtx2.setBounds(50, 250, 300, 30);
        cnt.add(jtx1);
        cnt.add(jtx2);
        cnt.add(cb);
        cnt.add(cb2);
        lbl1.setFont(fnt);
        lbl2.setFont(fnt);
        lbl3.setFont(fnt);
        jtx2.setEditable(false);
        cnt.add(lbl1);
        cnt.add(lbl2);
        cnt.add(lbl3);
        cnt.add(bt);
    }

    void calcCelcius() {
        a = Double.toString(result);
        jtx2.setText(a);
    }

    void calcKelvin() {
        ans = result + 273;
        a = Double.toString(ans);
        jtx2.setText(a);
    }

    void calcFahrenheit() {
        ans = result * (9.00 / 5.00) + 32;
        a = Double.toString(ans);
        jtx2.setText(a);
        result = 0;
    }

    void set_action() {
        jtx1.requestFocus();
        jtx1.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char ch = e.getKeyChar();
                if (!(ch >= '0' && ch <= '9')) {
                    Toolkit tk = Toolkit.getDefaultToolkit();
                    tk.beep();
                    e.consume();
                }
            }
        });
        try {
            cb.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    fromTemp = "" + cb.getItemAt(cb.getSelectedIndex());
                }
            });
            cb2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    toTemp = "" + cb2.getItemAt(cb2.getSelectedIndex());

                }
            });
            bt.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (fromTemp.equals(listTemp[0])) {
                        a = jtx1.getText();
                        result = Double.parseDouble(a);
                        conversion();
                    } else if (fromTemp.equals(listTemp[1])) {
                        a = jtx1.getText();
                        result = Double.parseDouble(a);
                        result = result - 273;
                        conversion();
                    } else if (fromTemp.equals(listTemp[2])) {
                        a = jtx1.getText();
                        result = Double.parseDouble(a);
                        result = (result - 32) * (5.00 / 9.00);
                        conversion();
                    }
                }
            });

        } catch (Exception e1) {

        }
    }

    void conversion() {
        if (toTemp.equals(listTemp[0])) {
            calcCelcius();
        } else if (toTemp.equals(listTemp[1])) {
            calcKelvin();
        } else if (toTemp.equals(listTemp[2])) {
            calcFahrenheit();
        }
    }

}

public class TesCal {

    public static void main(String[] args) {
        Convert temp = new Convert();
    }
}